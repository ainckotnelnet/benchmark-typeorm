import { User } from "./user";
import { getConnectionManager } from "typeorm";

const TENANTS = 80;
const ROUNDS = 20;

let current_round = 0;

console.log(`creating schema`);

getConnectionManager()
  .create({
    type: "postgres",
    host: "localhost",
    port: 5432,
    username: "test",
    password: "test",
    database: "test",
    entities: [User],
    synchronize: true,
    name: "abc"
  })
  .connect()
  .then(() => {
    console.log(`schema created`);
  })
  .then(async () => {
    while (current_round < ROUNDS) {
      console.log(`creating connection for the round ${current_round}`);
      const connections = [...Array(TENANTS).keys()].map(i =>
        getConnectionManager()
          .create({
            type: "postgres",
            host: "localhost",
            port: 5432,
            username: "test",
            password: "test",
            database: "test",
            entities: [User],
            synchronize: false,
            name: i.toString()
          })
          .connect()
      );
      const user = new User();
      user.firstName = "abcdefg";
      console.log(`saving`);
      await Promise.all(connections);
      await Promise.all(
        connections.map(c =>
          c.then(async con => {
            const u = await con.manager.save<User>(user);
            console.log(`user: ${u.id}`);
          })
        )
      );
      console.log(`closing`);
      await Promise.all(connections.map(c => c.then(con => con.close())));
      current_round++;
    }
  })
  .then(() => {
    const used = process.memoryUsage().heapUsed / (1024 * 1024);
    console.log(`Used ${used} MB`);
  });
